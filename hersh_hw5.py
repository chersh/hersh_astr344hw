print("Homework 5, Computational Astrophysics, Cora Hersh")

import numpy as np
import matplotlib.pyplot as plt

#make array of x values
X = []

for i in np.arange(-5, 7, 0.1):
    x = i
    X.append(x)
    
#define functions

def f(x):
    #roots from 2,4 is 3.14...
    return np.sin(x)

F = []

for x in X:
    f_x = f(x)
    F.append(f_x)

plt.plot(X, F)
plt.show()

def g(x):
    #root from 1,2 is 1.52138...
    return (x**3) - x - 2

G = []

for x in X:
    g_x = g(x)
    G.append(g_x)

plt.plot(X, G)
plt.show()

def y(x):
    #roots from 0,5 is 2
    return -6 + x + (x**2)

Y = []

for x in X:
    y_x = y(x)
    Y.append(y_x)

plt.plot(X, Y)
plt.show()

#define tolerance ~0

tolerance_toggle = input("Would you like to change tolerance from default 10^-3? (yes/no)")
if tolerance_toggle == str("yes"):
    tol = eval(input("New tolerance: "))
elif tolerance_toggle == str("no"):
    tol = 1 * 10**(-3)

#check to make sure root is bracketed (not as important)

#define function to find roots
def findroot(h, x1, x2):
    #if h(x1) and h(x2) are on the same side of the root, end process
    if h(x1) / h(x2) > 0.0:
        return str("Root not bracketed. Re-set brackets.")
    #if x1 is already close enough to root
    if abs(h(x1)) < tol:
        return x1, h(x1)
    #if x2 is already close enough to root
    elif abs(h(x2)) < tol:
        return x2, h(x2)
    else:
        xmid = (x1 + x2) / 2.0
        count = 0
        iterations_toggle = input("Would you like to change max iterations from default 100? (yes/no):")
        if iterations_toggle == str("yes"):
            max_iterations = eval(input("New max iterations: "))
        elif iterations_toggle == str("no"):
            max_iterations = 100
        while abs(h(xmid)) > tol:
            #stop process if taking too long
            count += 1
            if count > max_iterations:
                return str("Maximum iterations exceeded. Re-set brackets.")
            xmid = (x1 + x2) / 2.0
            if h(xmid) / h(x1) < 0.0:
               x2 = xmid
            elif h(xmid) / h(x2) < 0.0:
               x1 = xmid
        return xmid, h(xmid)
    
print ("root of f:", findroot(f, 2.0, 4.0))
print ("root of g:", findroot(g, 1.0, 2.0))
print ("root of y:", findroot(y, 1.5, 2.5))
