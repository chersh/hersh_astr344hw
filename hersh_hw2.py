print """
Homework #2
Cora Hersh
Computational Astrophysics
"""

import numpy as np

print "Calculate the recurrence relation for n = 0 through n=5 in single precision numbers:"

for i in range(6):
    x = np.float32((1.0 / 3.0)**i)
    print "n=", i, ":", x

print "Now try up to n=20:"

for i in range(21):
    x = np.float32((1.0 / 3.0)**i)
    print "n=", i, ":", x
    
print "Compute absolute error with respect to double precision numbers:"

for i in range(21):
    x = np.float64((1.0 / 3.0)**i) - np.float32((1.0 / 3.0)**i)
    print "n=", i, ":", x

print "Compute relative error with respect to double precision numbers:"

for i in range(21):
    x = (np.float64((1.0 / 3.0)**i) - np.float32((1.0 / 3.0)**i)) / (np.float64((1.0 / 3.0)**i))
    print "n=", i, ":", x

print "Try the same thing for x^n = 4^n."
print "Calculate the value of the formula from n=0 to n=20 in single precision:"

for i in range(21):
    x = np.float32(4.0**i)
    print "n=", i, ":", x

print "Compute absolute error with respect to double precision numbers:"
    
for i in range(21):
    x = np.float64(4.0**i) - np.float32(4.0**i)
    print "n=", i, ":", x

print "Compute relative error with respect to double precision numbers:"

for i in range(21):
    x= (np.float64(4.0**i) - np.float32(4.0**i)) / (np.float64(4.0**i))
    print "n=", i, ":", x

print "Why is this calculation stable?"
print """
>> This calculation is stable (at least up to n = 20) because single-precision
numbers can handle number up to 10^37, while 4^20 is only about 10^12. Also, 4^n
is always an integer as long as n is an integer and non-negative, unlike (1/3)^n,
which turns into an infinitely-long decimal number.
"""

print "Should absolute or relative error be used to measure accuracy and stability?"
print """
>> Relative error should be used to measure accuracy and stability. This is because
it shows you a value of error scaled to the size of the number you're interested in.
This is more useful than absolute error because it allows you to put the error
value in perspective of your overall calculations. It lets you decide whether the
error will affect your final numbers significantly or not.
"""
