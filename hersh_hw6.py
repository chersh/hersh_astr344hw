print """
Homework 6
Cora Hersh
Computational Astrophysics
"""

from datetime import datetime
from datetime import timedelta
import numpy as np
import matplotlib.pyplot as plt

print "Part One:"

#find roots for same functions as hw 5
#use the NR method

def deriv(m, x):
    dx = 10**(-5)
    hminus = x - dx
    hplus = x + dx
    m_x = m(x)
    m_xminus = m(hminus)
    m_xplus = m(hplus)
    d = (m_xplus - m_xminus) / (hplus - hminus)

    return d


def f(x):
    #root from 2,4 is 3.1415...
    return np.sin(x)

def g(x):
    #root from 1,2 is 1.52138...
    return (x**3) - x - 2

def y(x):
    #roots from 0,5 is 2
    return -6 + x + (x**2)

def findroot_nr(h, guess, tol):
    if abs(h(guess)) < tol:
        return guess, h(guess) 
    else:
        x = guess
        while abs(h(x)) > tol:
           x = x - (h(x) / deriv(h, x))
           #print h(x), x
        return x, h(x)

    
print "root of f with nr:", findroot_nr(f, 3.5, 10**(-3))
print "root of g with nr:", findroot_nr(g, 1.1, 10**(-3))
t1 = datetime.now()
print "root of y with nr:", findroot_nr(y, 3.0, 10**(-3))
t2 = datetime.now()
print "time taken to find root of y with nr:",  t2 - t1

#now use the bisection method again

def findroot(h, x1, x2, tol):
    #if h(x1) and h(x2) are on the same side of the root, end process
    if h(x1) / h(x2) > 0.0:
        return str("Root not bracketed. Re-set brackets.")
    #if x1 is already close enough to root
    if abs(h(x1)) < tol:
        return x1, h(x1)
    #if x2 is already close enough to root
    elif abs(h(x2)) < tol:
        return x2, h(x2)
    else:
        xmid = (x1 + x2) / 2.0
        while abs(h(xmid)) > tol:
            xmid = (x1 + x2) / 2.0
            if h(xmid) / h(x1) < 0.0:
               x2 = xmid
            elif h(xmid) / h(x2) < 0.0:
               x1 = xmid
        return xmid, h(xmid)

print "root of f with bisection:", findroot(f, 2.0, 4.0, 10**(-3))
print "root of g with bisection:", findroot(g, 1.0, 2.0, 10**(-3))
t3 = datetime.now()
print "root of y with bisection:", findroot(y, 0.0, 5.0, 10**(-3))
t4 = datetime.now()
print "time taken to find root of y with bisection:",  t4 - t3

print "The NR method tends to be faster than the bisection method."


print "Part Two:"

#define constants (in cgs units!!!)
h = 6.626075540 * 10**(-27) #ergs * s
c = 2.99792458 * 10**(10) #cm/s
v = c / (870.0 * 10**(-4)) # 1/s
k = 1.38065812 * 10**(-16) #erg/K

B = 1.25 * 10**(-12)


def z(T):
    return ((2.0 * h * (v**3 / c**2)) / (np.exp((h * v) / (k * T)) - 1) - B)


print "root of intensity function minus given value of intensity:", findroot_nr(z, 40.0, 10**(-25))

