print """
Homework 4
Cora Hersh
Computational Astrophysics
"""
import numpy as np
import matplotlib.pyplot as plt

#define constants
C = 3 * 10**3
omega_m = 0.3
omega_l = 0.7
omega_R = 0

#define H(z)
def H(z):
	return np.sqrt(omega_m * (1 + z)**3 + omega_R * (1 + z)**2 + omega_l)

#define r(z)
def r(z):
	return C / H(z)

#define trapezoid integration function
def trapz_integrate(x, y):
	integral = 0
	for i in range(len(x) - 1):
		integral += ((y[i+1] + y[i]) / 2) * (x[i+1] - x[i])
	return integral




#define the arrays to be used in the plot of z vs DA
Z = []
D = []

#Calculate multiple DA values and add them to a list (D). Also make a list of z_fin values in Z

for i in range(50+1):
	#define spacing between final z values
	z_fin = 0.1 * i
	Z.append(z_fin)
	
	#make arrays for the x and y points
	z_list = []
	y_list = []

	#define limits of integration and precision of integration
	#z_fin = 1.0
	z_init = 0
	dz = 0.01

	#create list of z points
	for i in range(int((z_fin - z_init) / dz) + 1):
		z = z_init + dz * i
		z_list.append(z)
	
	#create list of y points
	for z in z_list:
		y = r(z)
		y_list.append(y)
	#print "y1: ", y

	#show DA value
	#print "DA: ", trapz_integrate(z_list, y_list) / (1 + z)
	D.append(trapz_integrate(z_list, y_list) / (1 + z))


#Try doing the same thing, but now use the "canned" trapz function from numpy

#define the arrays to be used in the plot of z vs DA for the canned method
Z_can = []
D_can = []

#Calculate multiple DA values and add them to a list (D_can). Also make a list of z_fin values in Z_can

for i in range(50+1):
	#define spacing between final z values
	z_fin = 0.1 * i
	Z_can.append(z_fin)
	
	#make arrays for the x and y points
	z_list = []
	y_list = []

	#define limits of integration and precision of integration
	z_init = 0
	dz = 0.01

	#create list of z points
	for i in range(int((z_fin - z_init) / dz) + 1):
		z = z_init + dz * i
		z_list.append(z)
	
	#print int((z_fin - z_init) / dz) + 1
	
	#create list of y points
	for z in z_list:
		y = r(z)
		y_list.append(y)
	#print "y2: ", y_list
	
	#show DA value
	#print "DA: ", trapz_integrate(z_list, y_list) / (1 + z)
	D_can.append(np.trapz(y_list, z_list) / (1 + z))


print """Show plot of DA versus z for 'homemade' trapezoidal integrator 
function and 'canned' function: 'homemade' is represented by red dots,
while 'canned' is represented by a blue line.
"""

plt.plot(Z, D, 'ro', Z_can, D_can)
plt.xlabel('z2')
plt.ylabel('DA')
plt.show()


