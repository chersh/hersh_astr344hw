print """
Project 1
Cora Hersh
Computational Astrophysics
"""
import numpy as np
from numpy import random
import matplotlib.pyplot as plt
import pdb

#kdtree.ballpoint
#print type(my_x)
#pdb.set_trace()

#print i
#print "x: ", getattr(people[i], 'x')
#print "y: ", getattr(people[i], 'y')
#print "angle: ", getattr(people[i], 'angle')

dt = 1.0 #change in time per iteration
t = 0.0 #current time

N_people = 30
N_vaccinated = 3

people = []

infection_radius = 0.1
personal_space = 0.06
 
class Person:

    infection_count = 1 #always starts with one infected person
    
    def __init__(self, x, y, v, angle, infected, vaccinated): #add another input: vaccinated
        self.x = x #np.random.random()
        self.y = y #np.random.random()
        self.v = v
        self.angle = angle
        self.infected = infected
        self.vaccinated = vaccinated
        
    def move(self):
        dx = self.v * np.cos(self.angle) * dt
        dy = self.v * np.sin(self.angle) * dt
        if (self.x + dx) > 1.0 or (self.y + dy) > 1.0 or (self.x + dx) < 0.0 or (self.y + dy) < 0.0:
            self.angle = 2 * np.pi * np.random.random()
        else:
            self.x = self.x + dx
            self.y = self.y + dy

    def infect(self):
        if self.infected == False:
            self.infected = True
            Person.infection_count += 1

    def avoid(self):
        self.angle = 2 * np.pi * np.random.random()
        


patient_zero = Person(np.random.random(), np.random.random(), 0.1, 2 * np.pi * np.random.random(), True, False)  #infected
people.append(patient_zero)

for i in range(N_vaccinated):
    p = Person(np.random.random(), np.random.random(), 0.1, 2 * np.pi * np.random.random(), False, True) #vaccinated
    people.append(p)
    
for i in range(N_people - N_vaccinated - 1):
    p = Person(np.random.random(), np.random.random(), 0.1, 2 * np.pi * np.random.random(), False, False) #not infected --yet
    people.append(p)

#for k in range(30):
while Person.infection_count < N_people - N_vaccinated:
    infected_x_positions = []
    healthy_x_positions = []
    vaccinated_x_positions = []
   
    infected_y_positions = []
    healthy_y_positions = []
    vaccinated_y_positions = []
    
    for i in range(N_people):

        #infect a person if close to another person who is infected
        my_x = people[i].x
        if people[i].infected == True:
            infected_x_positions.append(my_x)
        elif people[i].infected == False and people[i].vaccinated == False:
            healthy_x_positions.append(my_x)
        elif people[i].infected == False and people[i].vaccinated == True:
            vaccinated_x_positions.append(my_x)
            
        my_y = people[i].y
        if people[i].infected == True:
            infected_y_positions.append(my_y)
        elif people[i].infected == False and people[i].vaccinated == False:
            healthy_y_positions.append(my_y)
        elif people[i].infected == False and people[i].vaccinated == True:
            vaccinated_y_positions.append(my_y)
        
        for j in range(N_people):
            other_x = people[j].x
            other_y = people[j].y
            #pdb.set_trace()
            probi = np.random.random()
            distance =  float(((my_x - other_x)**2. + (my_y - other_y)**2.)**(0.5))
            if j != i and distance < infection_radius and people[j].infected == True and people[i].vaccinated == False and probi < 0.75:
                people[i].infect()
            """
            deltax = people[i].v * np.cos(people[i].angle) * dt
            deltay = people[i].v * np.sin(people[i].angle) * dt
            next_distance = float(((my_x + deltax - other_x)**2. + (my_y + deltay - other_y)**2.)**(0.5))
            if next_distance < personal_space:
                people[i].avoid
            """
        print i, "infected? ", people[i].infected
        people[i].move()

    plt.plot(infected_x_positions, infected_y_positions, 'ro', healthy_x_positions, healthy_y_positions, 'bo', vaccinated_x_positions, vaccinated_y_positions, 'go' )
    plt.show()
    
    #print "Time = ", t
    #print "Total people infected: ", Person.infection_count

    #if Person.infection_count == (N_people - N_vaccinated):
    #   print "Time to get everyone sick who can get sick: ",  Person.infection_count
    
    t = t + dt
print "Time to get everyone sick who can get sick: ", t
    

"""
#write a random number generator that draws from an exponential distribution

#use random.random and transform it

random_numbers = []

for i in range(100):
    number = -1.0 * np.log(1.0 - np.random.random())
    random_numbers.append(number)
    
#print random_numbers
"""
