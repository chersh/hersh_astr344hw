print """
Homework 7, Part 2
Cora Hersh
Computational Astrophysics
"""
import numpy as np
from numpy import random

#have "people walking into a room" one by one
#stop loop when two birthdays match, record number of people in room
#do this N times, make plot of group sizes where a bday match was made
#the 50th percentile should be n=23
#when person walks in, generate random birthday, see if that bday "in" list of bdays
#if yes, add current number of people to list of group sizes
#if no, add another person

N = 100000
list_of_group_sizes = []
    

for i in range(N):
    birthdays = []
    group_size = 0

    dayinbdays = False
    
    day = int(364 * np.random.random_sample()) + 1
    
    while dayinbdays == False:
        birthdays.append(day)
        day = int(364 * np.random.random_sample()) + 1
        group_size += 1
        dayinbdays = day in birthdays
       
    list_of_group_sizes.append(group_size + 1) #+1 because will stop before counting the last person

print "50th percentile of group sizes with a birthday match: ", np.median(list_of_group_sizes)
