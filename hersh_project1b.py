print """
Project 1, part a
Cora Hersh
Computational Astrophysics


How long does it take for every infectable person to get sick
when different numbers of people are vaccinated?
"""
import numpy as np
from numpy import random
import matplotlib.pyplot as plt
import pdb

dt = 1.0 #change in time per iteration
t = 0.0 #current time

N_people = 100
#N_vaccinated = 0

infection_radius = 0.1
#personal_space = 0.06
 
class Person:

    infection_count = 1 #always starts with one infected person
    
    def __init__(self, x, y, v, angle, infected, vaccinated): #add another input: vaccinated
        self.x = x #np.random.random()
        self.y = y #np.random.random()
        self.v = v
        self.angle = angle
        self.infected = infected
        self.vaccinated = vaccinated
        
    def move(self):
        dx = self.v * np.cos(self.angle) * dt
        dy = self.v * np.sin(self.angle) * dt
        if (self.x + dx) > 3.0 or (self.y + dy) > 3.0 or (self.x + dx) < 0.0 or (self.y + dy) < 0.0:
            self.angle = 2 * np.pi * np.random.random()
        else:
            self.x = self.x + dx
            self.y = self.y + dy

    def infect(self):
        if self.infected == False:
            self.infected = True
            Person.infection_count += 1

print "Run the simulation 100 times and average the times it takes to get everyone sick."
print "Do this for number of vaccinated people from 0 to 98."

#array to hold numbers of people vaccinated
number_vaccines = []
for i in range(0,96,5):
    number_vaccines.append(i)

#array to hold average runtime for each number vaccines
average_times = []


#the big for loop...

for N_vaccinated in range(0,96,5):
    #make array to hold all the simulation runtimes
    times = []
    
    for l in range(25):
    
        people = [] #define array to hold people
    
        t = 0.0
        Person.infection_count = 1
    
        patient_zero = Person(np.random.random(), np.random.random(), 0.1, 2 * np.pi * np.random.random(), True, False)  #infected
        people.append(patient_zero)

        #print patient_zero.x
        #print patient_zero.y
    
        for i in range(N_vaccinated):
            p = Person(np.random.random(), np.random.random(), 0.1, 2 * np.pi * np.random.random(), False, True) #vaccinated
            people.append(p)
    
        for i in range(N_people - N_vaccinated - 1):
            p = Person(np.random.random(), np.random.random(), 0.1, 2 * np.pi * np.random.random(), False, False) #not infected --yet
            people.append(p)

        while Person.infection_count < N_people - N_vaccinated:
            infected_x_positions = []
            healthy_x_positions = []
            vaccinated_x_positions = []
   
            infected_y_positions = []
            healthy_y_positions = []
            vaccinated_y_positions = []
    
            for i in range(N_people):
            #infect a person if close to another person who is infected
                my_x = people[i].x
                if people[i].infected == True:
                    infected_x_positions.append(my_x)
                elif people[i].infected == False and people[i].vaccinated == False:
                    healthy_x_positions.append(my_x)
                elif people[i].infected == False and people[i].vaccinated == True:
                    vaccinated_x_positions.append(my_x)
            
                my_y = people[i].y
                if people[i].infected == True:
                    infected_y_positions.append(my_y)
                elif people[i].infected == False and people[i].vaccinated == False:
                    healthy_y_positions.append(my_y)
                elif people[i].infected == False and people[i].vaccinated == True:
                    vaccinated_y_positions.append(my_y)
        
                for j in range(N_people):
                    other_x = people[j].x
                    other_y = people[j].y
                    probi = np.random.random()
                    distance =  float(((my_x - other_x)**2. + (my_y - other_y)**2.)**(0.5))
                    if j != i and distance < infection_radius and people[j].infected == True and people[i].vaccinated == False and probi < 0.75:
                        people[i].infect()
                    
                #print i, "infected? ", people[i].infected
                people[i].move()

            t = t + dt
            #print "infection count: ", Person.infection_count
        
        #print "Time to get everyone sick: ", t
        times.append(t)

    print "average time to get everyone sick: ", np.average(times)
    average_times.append(np.average(times))
    
#print average_times


plt.plot(number_vaccines, average_times, 'go')
plt.title('Relationship between number of vaccinated people and time to infect everyone')
plt.xlabel('Number of vaccinated people')
plt.ylabel('Average time to get everyone sick who can get sick in a 3x3 room')
plt.show()
