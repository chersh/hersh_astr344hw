print """
Homework 7, Part 1
Cora Hersh
Computational Astrophysics
"""

import numpy as np
from numpy import random

#calculate pi using hit or miss.
#show how your answer converges with increasing sample sizes to 3.141


inside = 0
outside = 0

N = 10000000

for i in range(N):
    x = 2 * np.random.random_sample() -1
    y = 2 * np.random.random_sample() -1
    if x**2 + y**2 < 1:
        inside += 1
    if x**2 + y**2 > 1:
        outside += 1
        
print "inside: ", float(inside)
print "outside: ", float(outside)

print "percent inside: ", float(inside) / float(N)
print "value of pi: ", 4.0 * (float(inside) / float(N))

print """
It takes N as at least as large as 10,000,000 to get the digits 3.141
for the value of pi. For N = 100,000, pi is evaluated as 3.150.
For N = 1,000, pi is evaluated as 3.152. For N = 100, pi is evaluated as 3.2.
"""
