print """
Homework #3
Cora Hersh
Computational Astrophysics
"""

import numpy as np

import math as math

print "Convert number counts to differential counts for the model:"

#load in model and data files, call columns x and y (or X and Y)

x, y = np.loadtxt('astr344_homework_materials/model_smg.dat', unpack = True)

X, Y = np.loadtxt('astr344_homework_materials/ncounts_850.dat', unpack = True)

#make an array for derivatives of f
df = []

for i in range(1, 11):
	hminus = x[i] - x[i-1]
	hplus = x[i+1] - x[i]
	f_xi = y[i]
	f_xminus = y[i-1]
	f_xplus = y[i+1]
	
	d = abs((hminus / (hplus * (hminus + hplus))) * f_xplus - ((hminus - hplus) / (hplus * hminus)) * f_xi - (hplus / (hminus * (hminus + hplus))) * f_xminus)
	
	df.append(d)
	
	print "df: ", d

print "Take the log of the derivatives so they match the scale of the data file."

#make an array for the log base 10 of derivatives of f
df_log = []

#convert derivatives of number of galaxies to log scale
for i in range (10):
	d_log = math.log10(df[i])
	
	df_log.append(d_log)
	
	print d_log

print "Take the log of the luminosity values so they match the scale of the data file."

#make an array for the log base 10 of model luminosity
model_luminosity_log = []

#convert model luminosity to log scale
for i in range (1, 11):
	luminosity_log = math.log10(x[i])
	
	model_luminosity_log.append(luminosity_log)
	
	print luminosity_log

print "Plot the results against the data!"

import matplotlib.pyplot as plt

plt.plot(X, Y, 'ro', model_luminosity_log, df_log, 'b-')
plt.ylabel('Galaxy counts (log scale)')
plt.xlabel('Luminosity (log scale)')
plt.show()
